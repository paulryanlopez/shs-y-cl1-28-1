package com.iacademy.midterms_lopez;

import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public void eatCookie(View view){
        TextView textView = findViewById(R.id.textView);
        textView.setText("I'm so full");
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.after_cookie);
    }
    public void reset(View view){
        TextView textView = findViewById(R.id.textView);
        textView.setText("I'm so hungry");
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.before_cookie);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
